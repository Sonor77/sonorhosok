package com.company;

import com.company.Locations.LocationDriver;
import com.company.Locations.Map;
import com.company.Locations.MapVo;

import java.util.ArrayList;

public class StatUpdate {
    void WeaponUpdate(){
        //PC
        Champion.getInstance().setTotalDamage(Champion.getBaseDamage() + Champion.getWeaponDamageModifier());//BaseDamage + WeaponDamageModifier;
        Champion.getInstance().setTotalDefence(Champion.getBaseDefence());// BaseDefence + ArmorModifier;
        Champion.getInstance().setTotalHitChance(Champion.getBaseHitChance() + Champion.getWeaponHitChanceModifier()) ;// BaseHitChance + WeaponHitChanceModifier;
        Champion.getInstance().setTotalCritChance(Champion.getBaseCritChance() + Champion.getWeaponCritChanceModifier());// BaseCritChance + WeaponCritChanceModifier;

        //Mob
        MobRandomizer.EnemyTotalDamage = MobRandomizer.EnemyBaseDamage;//BaseDamage + WeaponDamageModifier;
        MobRandomizer.EnemyTotalDefence = MobRandomizer.EnemyBaseDefence;// BaseDefence + ArmorModifier;
        MobRandomizer.EnemyTotalHealth = MobRandomizer.EnemyCurrentHealth;// BaseHealth + HealthModifiers;
        MobRandomizer.EnemyTotalHitChance = MobRandomizer.EnemyBaseHitChance;// BaseHitChance + WeaponHitChanceModifier;
        MobRandomizer.EnemyTotalCritChance = MobRandomizer.EnemyBaseCritChance;// BaseCritChance + WeaponCritChanceModifier;
    }
    public static void HealUp(){
        Champion.setCurrentHealth(Champion.getMaxHealth());// BaseHealth + HealthModifiers;
    }
    static void emergencyHealUp(int currentHealth, ArrayList<MapVo> Map){
        Double emergencyBar = 0.3;
        Double emergency = Champion.getMaxHealth() * emergencyBar;
        int emergencyLevel = emergency.intValue();

        if (currentHealth < emergencyLevel){
            System.out.println(Main.ANSI_YELLOW+"You need a drink. To the pub!"+Main.ANSI_RESET);
            new LocationDriver().scanNeighbourPos(new LocationDriver().getCurrentPos(), Map);
        }
    }
    public void mapSetter(String setMapTo){
        if(setMapTo == "ForestLake"){
            new DBUpdaters().getMapsFromDB("forestlakemap"); //load the map into the array first
            Adventure.currentMap = LocationDriver.ForestLakeMap; //load the map into the adventure array
            Main.mapChoice = "ForestLake";
        }
        else if (setMapTo == "CityMap"){
            new DBUpdaters().getMapsFromDB("citymap");
            Adventure.currentMap = LocationDriver.CityMap;
            Main.mapChoice = "CityMap";
        }
    }
}
