package com.company.Locations;

import com.company.StatUpdate;

public class Pub implements Locations {
    Pub(){ StatUpdate.HealUp();}

    int setNumber = 2;
    String setType = "Pub";
    @Override
    public void Introduction() {
        System.out.println("You find a pub during your journey, and decide to rest");
    }

    @Override
    public int getSetNumber() {
        return this.setNumber;
    }

    @Override
    public void setSetNumber() {

    }

    @Override
    public String getSetType() {
        return this.setType;
    }

    @Override
    public void setSetType() {

    }
}
