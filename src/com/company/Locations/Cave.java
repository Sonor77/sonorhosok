package com.company.Locations;

public class Cave implements Locations {
    int setNumber = 0;
    String setType = "Cave";
    @Override
    public void Introduction() {
        System.out.println("You enter a dark cave");
    }

    @Override
    public int getSetNumber() {
        return this.setNumber;
    }

    @Override
    public void setSetNumber() {
    }

    @Override
    public String getSetType() {
        return this.setType;
    }

    @Override
    public void setSetType() {
    }
}
