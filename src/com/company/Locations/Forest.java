package com.company.Locations;

public class Forest implements Locations {
    int setNumber = 1;
    String setType = "Forest";
    @Override
    public void Introduction() {
        System.out.println("You enter a dark forest");
    }

    @Override
    public int getSetNumber() {
        return this.setNumber;
    }

    @Override
    public void setSetNumber() {

    }

    @Override
    public String getSetType() {
        return this.setType;
    }

    @Override
    public void setSetType() {

    }
}
