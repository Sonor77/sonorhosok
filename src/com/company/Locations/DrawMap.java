package com.company.Locations;
import com.company.Main;
import com.company.Misc.CrappyMapMissingException;

import java.util.ArrayList;

import static com.company.Main.*;

public class DrawMap {
    public void drawMap(){ //x a magasság, y a szélesség
        int xCoordinate = 1;
        int yCoordinate;
        int xcurrentPos = new LocationDriver().getCurrentPosX();
        int ycurrentPos = new LocationDriver().getCurrentPosY();
        boolean champPos;
        ArrayList<String> locals = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {//the first local in the list is x1 y0
            yCoordinate = 1;
            while(yCoordinate <= 10){
                champPos = champPosValidator(xCoordinate, yCoordinate, xcurrentPos, ycurrentPos);
                if (champPos == true) {
                    locals.add("H");
                    yCoordinate++;
                } else {
                    locals.add(searchForLocal(xCoordinate, yCoordinate));
                    yCoordinate++;
                }
            }
            mapDrawer(replaceLocalinMap(locals));
            locals.clear();
            xCoordinate++;
        }
        }
    public void drawForestLakeMap(){ //x a magasság, y a szélesség
        int xCoordinate = 1;
        int yCoordinate;
        int xcurrentPos = new LocationDriver().getCurrentPosX();
        int ycurrentPos = new LocationDriver().getCurrentPosY();
        boolean champPos;
        ArrayList<String> locals = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {//the first local in the list is x1 y0
            yCoordinate = 1;
            while(yCoordinate <= 10){
                champPos = champPosValidator(xCoordinate, yCoordinate, xcurrentPos, ycurrentPos);
                if (champPos == true) {
                    locals.add("H");
                    yCoordinate++;
                } else {
                    locals.add(searchForForestLakeLocal(xCoordinate, yCoordinate));
                    yCoordinate++;
                }
            }
            mapDrawer(replaceLocalinMap(locals));
            locals.clear();
            xCoordinate++;
        }
    }

    private void mapDrawer(ArrayList<String> locals){
        System.out.println(locals);
    }
    private String searchForLocal(int x, int y){
        String local;
        int index = 0;
        try {
            index = new LocationDriver().returnIndex(x,y, new LocationDriver().getCurrentActiveMap());
        } catch (CrappyMapMissingException e) {
            e.printStackTrace();
        }
        local = new LocationDriver().getCurrentActiveMap().get(index).local;
        return local;

    }
    private String searchForForestLakeLocal(int x, int y){
        String local;
        int index = 0;
        try {
            index = new LocationDriver().returnIndex(x,y, new LocationDriver().getCurrentActiveMap());
        } catch (CrappyMapMissingException e) {
            e.printStackTrace();
        }
        local = LocationDriver.ForestLakeMap.get(index).local;
        return local;

    }
    private ArrayList<String> replaceLocalinMap(ArrayList<String> locals) {
        ArrayList<String> returnLocal = new ArrayList<>();
        for (String local:locals) {
            switch (local) {
                case "Cave":
                    returnLocal.add(Main.ANSI_WHITE+"^"+Main.ANSI_RESET);
                    break;
                case "Forest":
                    returnLocal.add(Main.ANSI_GREEN+"%"+Main.ANSI_RESET);
                    break;
                case "Tower":
                    returnLocal.add(Main.ANSI_CYAN+"|"+Main.ANSI_RESET);
                    break;
                case "Pub":
                    returnLocal.add(Main.ANSI_YELLOW+"ß"+ANSI_RESET);
                    break;
                case "Lake":
                    returnLocal.add(Main.ANSI_BLUE+"O"+Main.ANSI_RESET);
                    break;
                case "Water":
                    returnLocal.add(ANSI_BLUE+"0"+Main.ANSI_RESET);
                    break;
                case "Roads":
                    returnLocal.add("I");
                    break;
                case "H":
                    returnLocal.add(Main.ANSI_PURPLE+"H"+Main.ANSI_RESET);
            }

        }
        return returnLocal;
    }
    private String replaceLocalinMapWithHero(String local) {
        String returnLocal = "";
        switch (local) {
                case "Cave":
                    returnLocal =(ANSI_PURPLE+"^"+Main.ANSI_RESET);
                    break;
                case "Forest":
                    returnLocal = (ANSI_PURPLE+"%"+Main.ANSI_RESET);
                    break;
                case "Tower":
                    returnLocal =(ANSI_PURPLE+"|"+Main.ANSI_RESET);
                    break;
                case "Pub":
                    returnLocal = (ANSI_PURPLE+"ß"+Main.ANSI_RESET);
                    break;
                case "Lake":
                    returnLocal =(ANSI_PURPLE+"O"+Main.ANSI_RESET);
                    break;
            }
        return returnLocal;
    }
    private boolean champPosValidator(int xCoordinate, int yCoordinate, int xcurrentPos, int ycurrentPos){
        if (xCoordinate == xcurrentPos && yCoordinate == ycurrentPos) {
            return true;
        }
        else{return false;}
    }
}
