package com.company.Locations;

import java.util.ArrayList;

public class MapVo {
    int x;
    int y;
    String local;

    public int getXLocation(int mapArrayNumber, ArrayList<MapVo> Map){
        int x = Map.get(mapArrayNumber).x;
        return x;
    }
    public int getYLocation(int mapArrayNumber,ArrayList<MapVo> Map){
        int y = Map.get(mapArrayNumber).y;
        return y;
    }
    public String getLocal(int mapArrayNumber,ArrayList<MapVo> Map){
        String local = Map.get(mapArrayNumber).local;
        return local;
    }
    public int getNextLocal(int nextX, int nextY, ArrayList<MapVo> Map){
        int nextLocalNum = 0;
        int len = Map.size();
        for (int i = 0; i < len; i++){
            if(Map.get(i).getXLocation(i,Map) == nextX && Map.get(i).getYLocation(i,Map) == nextY){
                nextLocalNum = i;
            }
            //else{System.out.println(LocationDriver.Map.get(i).getXLocation(i) + LocationDriver.Map.get(i).getYLocation(i));}
        }

        return nextLocalNum;
    }
    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }
    public void setLocal(String local){
        this.local = local;
    }

}
