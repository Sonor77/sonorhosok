package com.company;

import com.company.Misc.Debugger;
import com.company.WeaponList.*;

public class Inventory implements Weapons {
    public static boolean WeaponEquipped = false;
    public static int ClubAmount;
    public static int BronzeSwordAmount;
    public static int SteelSwordAmount;
    public static int DaggerAmount;
    public static int WoodBowAmount;
    public static int EbonyBowAmount;
    public static int GlassBowAmount;
    public static int WoodStaffAmount;
    public static int EbonyStaffAmount;
    //Equipped weapons
    public static boolean ClubEquipped = false;
    public static boolean BronzeSwordEquipped = false;
    public static boolean SteelSwordEquipped = false;
    public static boolean DaggerEquipped = false;
    public static boolean WoodBowEquipped = false;
    public static boolean EbonyBowEquipped = false;
    public static boolean GlassBowEquipped = false;
    public static boolean WoodStaffEquipped = false;
    public static boolean EbonyStaffEquipped = false;

    @Override
    public int getDamage() { return Champion.getInstance().getEquippedWeapon().getDamage();
    }

    @Override
    public int getCrit() { return Champion.getInstance().getEquippedWeapon().getCrit();
    }

    @Override
    public int getCritDmgModifier() {
        return Champion.getInstance().getEquippedWeapon().getCritDmgModifier();
    }

    @Override
    public int getWeaponLevel() {
        return Champion.getInstance().getEquippedWeapon().getWeaponLevel();
    }

    @Override
    public String getWeaponName() { return Champion.getInstance().getEquippedWeapon().getWeaponName();
    }

    //Add Items to use
    void AddClub(){
        if (ClubAmount > 0) {
            addWeapon(new Club());
            ClubAmount--;
            ClubEquipped = true;
        }

    }
    void AddBronzeSword(){
        if (BronzeSwordAmount > 0) {
            addWeapon(new BronzeSword());
            BronzeSwordAmount--;
            BronzeSwordEquipped = true;
        }

    }
    void AddSteelSword(){
        if (SteelSwordAmount > 0) {
            addWeapon(new SteelSword());
            SteelSwordAmount--;
            SteelSwordEquipped = true;
        }
    }
    void AddDagger(){
        if (DaggerAmount > 0) {
            addWeapon(new Dagger());
            DaggerAmount--;
            DaggerEquipped = true;
        }

    }
    void AddWoodStaff(){
        if (WoodStaffAmount > 0) {
            addWeapon(new WoodStaff());
            WoodStaffAmount--;
            WoodStaffEquipped = true;
        }

    }
    void AddEbonyStaff(){
        if (EbonyStaffAmount > 0) {
            addWeapon(new EbonyStaff());
            EbonyStaffAmount--;
            EbonyStaffEquipped = true;
        }

    }
    void AddWoodBow(){
        if (WoodBowAmount > 0) {
            addWeapon(new WoodenBow());
            WoodBowAmount--;
            WoodBowEquipped = true;
        }

    }
    void AddEbonyBow(){
        if (EbonyBowAmount > 0) {
            addWeapon(new EbonyBow());
            EbonyBowAmount--;
            EbonyBowEquipped = true;
        }

    }
    void AddGlassBow(){
        if (GlassBowAmount > 0) {
            addWeapon(new GlassBow());
            GlassBowAmount--;
            GlassBowEquipped = true;
        }

    }
    //Remove Items
    void RemoveClub(){if (ClubEquipped == true){
        removeWeapon();
        ClubEquipped = false;
    }}
    void RemoveBronzeSword(){ if (BronzeSwordEquipped == true) {
        removeWeapon();
        BronzeSwordEquipped = false;
    }}
    void RemoveSteelSword(){if (SteelSwordEquipped == true) {
        removeWeapon();
        SteelSwordEquipped = false;
    }}
    void RemoveDagger(){if (DaggerEquipped == true) {
        removeWeapon();
        DaggerEquipped = false;
    }}
    void RemoveWoodStaff(){if (WoodStaffEquipped == true) {
        removeWeapon();
        WoodStaffEquipped = false;
    }}
    void RemoveEbonyStaff(){if (EbonyStaffEquipped == true) {
        removeWeapon();
        EbonyStaffEquipped = false;
    }}
    void RemoveWoodBow(){if (WoodBowEquipped == true) {
        removeWeapon();
        WoodBowEquipped = false;
    }}
    void RemoveEbonyBow(){if (EbonyBowEquipped == true) {
        removeWeapon();
        EbonyBowEquipped = false;
    }}
    void RemoveGlassBow(){if (GlassBowEquipped == true) {
        removeWeapon();
        GlassBowEquipped = false;
    }}
    //Két osztály kell, ami eldönti h az új fegyver jobb-e, és a másik ami felteszi
    void AutoEquip() {
        if (WeaponEquipped == true && Fight.nextWeaponLevel > Champion.getInstance().getEquippedWeapon().getWeaponLevel()) {//itt dől el h jobb-e a fegyver
            String equippedWeapon = Champion.getInstance().getEquippedWeapon().getWeaponName();
            switch (equippedWeapon){ //Removes the currently equipped weapon
                case "Club": RemoveClub();break;
                case "BronzeSword": RemoveBronzeSword();break;
                case "SteelSword": RemoveSteelSword();break;
                case "Dagger": RemoveDagger();break;
                case "WoodenBow":RemoveWoodBow();break;
                case "EbonyBow":RemoveEbonyBow();break;
                case "GlassBow":RemoveGlassBow();break;
                case "WoodStaff":RemoveWoodStaff();break;
                case "EbonyStaff":RemoveEbonyStaff();break;
            }
        }
        if (WeaponEquipped == false) { //starts the equip process
            BarbEquipper();
            NecromancerEquipper();
            RangerEquipper();
        } //equips weapons that are found
    }
    String WhatIsEquipped(){
        String WhatIsEquipped = "empty";
        if (WeaponEquipped == true){
            if (ClubEquipped == true){WhatIsEquipped = "Club";}
            else if (BronzeSwordEquipped == true){WhatIsEquipped = "BronzeSword";}
            else if (SteelSwordEquipped == true){WhatIsEquipped = "SteelSword"; }
            else if (DaggerEquipped == true){WhatIsEquipped = "Dagger"; }
            else if (WoodBowEquipped == true){WhatIsEquipped = "WoodenBow"; }
            else if (EbonyBowEquipped == true){WhatIsEquipped = "EbonyBow"; }
            else if (GlassBowEquipped == true){WhatIsEquipped = "GlassBow"; }
            else if (WoodStaffEquipped == true){WhatIsEquipped = "WoodStaff"; }
            else if (EbonyStaffEquipped == true){WhatIsEquipped = "EbonyStaff"; }
        }
        return WhatIsEquipped;
    }//decides on what the champ is wearing
    void BarbEquipper(){           if (Main.ChampChoice == 1) {
        if (SteelSwordAmount > 0 && SteelSwordEquipped == false) {
            AddSteelSword();
            WeaponEquipped = true;
            EquipFlavour("a steel Sword");
            //new Debugger().printToFile("Steel Sword Equipped");
        }
    }
        if (Main.ChampChoice == 1) {
            if (BronzeSwordAmount > 0 && BronzeSwordEquipped == false) {
                AddBronzeSword();
                WeaponEquipped = true;
                EquipFlavour("a bronze Sword");
                //new Debugger().printToFile("Bronze Sword Equipped");
            }
        }
        if (Main.ChampChoice == 1) {
            if (ClubAmount > 0 && ClubEquipped == false) {
                AddClub();
                WeaponEquipped = true;
                EquipFlavour("a club");
                //new Debugger().printToFile("Club Equipped");
            }
        }}
    void NecromancerEquipper(){   if (Main.ChampChoice == 2) {
        if (DaggerAmount > 0 && DaggerEquipped == false) {
            AddDagger();
            WeaponEquipped = true;
            EquipFlavour("a dagger");
            //new Debugger().printToFile("Dagger Equipped");
        }
        if (WoodStaffAmount > 0 && WoodStaffEquipped == false) {
            AddWoodStaff();
            WeaponEquipped = true;
            EquipFlavour("a wood Staff");
            //new Debugger().printToFile("Wood Staff Equipped");
        }
        if (EbonyStaffAmount > 0 && EbonyStaffEquipped == false) {
            AddEbonyStaff();
            WeaponEquipped = true;
            EquipFlavour("an ebony Staff");
            //new Debugger().printToFile("Ebony Staff Equipped");
        }
    }}
    void RangerEquipper(){
        if (Main.ChampChoice == 3) {
            if (WoodBowAmount > 0 && WoodBowEquipped == false) {
                AddWoodBow();
                WeaponEquipped = true;
                EquipFlavour("a wooden Bow");
                //new Debugger().printToFile("Wood Bow Equipped");
            }
        }
        if (Main.ChampChoice == 3) {
            if (EbonyBowAmount > 0 && EbonyBowEquipped == false) {
                AddEbonyBow();
                WeaponEquipped = true;
                EquipFlavour("an ebony Bow");
                //new Debugger().printToFile("Ebony Bow Equipped");
            }
        }
        if (Main.ChampChoice == 3) {
            if (GlassBowAmount > 0 && GlassBowEquipped == false) {
                AddGlassBow();
                WeaponEquipped = true;
                EquipFlavour("a glass Bow");
                //new Debugger().printToFile("Glass Bow Equipped");
            }
        }
    }

    void removeWeapon(){
        Champion.setWeaponDamageModifier(0);
        Champion.setWeaponCritChanceModifier(0);
        Champion.setWeaponCritDamageModifier(0);
        WeaponEquipped = false;
    }

    void addWeapon(Weapons weapon){
        Champion.getInstance().equipWeapon(weapon);//new és a weapon ami kéne

        int dmgToAdd = weapon.getDamage();
        int critToAdd = weapon.getCrit();
        int critModToADD = weapon.getCritDmgModifier();

        Champion.getInstance().setWeaponDamageModifier(dmgToAdd);
        Champion.getInstance().setWeaponCritChanceModifier(critToAdd);
        Champion.getInstance().setWeaponCritDamageModifier(critModToADD);

        new StatUpdate().WeaponUpdate();

    }
    void EquipFlavour(String flavour){System.out.println("You have equipped "+flavour);}

}

