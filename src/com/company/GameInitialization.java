package com.company;

import com.company.Locations.LocationDriver;

public class GameInitialization {
    public void Intro(){
        System.out.println("Welcome to Hősök 2");
    }
    public void InitializationComplete(){
        System.out.println("Initialization is complete");
    }
    public void mapGeneration(String startingPosition){
        if(startingPosition == "ForestLake") {
            int lakeBorder = new LocationDriver().decideLakeBorder();
            new LocationDriver().generateMapForestLake(lakeBorder);
            new LocationDriver().defaultPos(new LocationDriver().getCurrentActiveMap());
        }
        else if (startingPosition == "CityMap"){
            new LocationDriver().generateMapCity();
            new LocationDriver().defaultPos(new LocationDriver().getCurrentActiveMap());
        }
    }
}
