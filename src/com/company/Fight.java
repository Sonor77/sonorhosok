package com.company;

import com.company.Misc.WeaponAlreadyOwnedException;
import com.company.WeaponList.BronzeSword;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Fight implements Weapons {
    public static int DeadEnemyCounter;
    public static int LootDifficulty;
    public static int nextWeaponLevel;

    Weapons weapon;

    public void FightInit(){
        //Randomize a monster
    }
    //Fight
    Fight(){//az értékek maniplulációja a Centerben van
        boolean spellCastHappened = false;
        new Center().Init();
        while (MobRandomizer.EnemyCurrentHealth > 0 && Champion.getCurrentHealth() > 0 && Main.CharacterIsALive == true) {
            if (Champion.getChampClass() == "Necromancer" && spellCastHappened == false){
            String spellToCast = InputScanner.inputScanner("Do you want to cast a spell?");
            Spells.Cast(spellToCast);
            spellCastHappened = true;
            }
            if (Champion.getChampClass() == "Necromancer" && Pet.petExsists == true){
                Center.Battle(Center.RandomHit(), new Center().RandomCrit(), new Center().RandomEnemyCrit(), Pet.petBaseHitChance, Pet.petBaseCritChance, Pet.petBaseDamage);
            }else {
            Center.Battle(Center.RandomHit(), new Center().RandomCrit(), new Center().RandomEnemyCrit());}
            try { Thread.sleep(1000);
            } catch (InterruptedException e) { e.printStackTrace(); }
            if (Champion.getCurrentHealth() <= 0) {//Itt hal meg ha itt az ideje
                Die();
                return;
            }
        }
        if (MobRandomizer.EnemyCurrentHealth <= 0) {
            System.out.println(Main.ANSI_GREEN+"The enemy has been defeated."+Main.ANSI_RESET);
            Ascend.AcquireXP(MobRandomizer.EnemyXP);
            Fight.DeadEnemyCounter++;
            try {
                TimeUnit.SECONDS.sleep(1);
            }catch (java.lang.InterruptedException ex){ Thread.currentThread().interrupt(); }
            LootRandomizer();
            new DBUpdaters().champDeadEnemyCounterUpdater(DeadEnemyCounter);
        }
    }
    void Loot() throws WeaponAlreadyOwnedException {//Find a weapon or armor
        int WhatToLoot = randomNumberGen(1,9);
        switch (WhatToLoot) {
            case 1:
                if (Inventory.ClubAmount < 1){
                Inventory.ClubAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Club"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 2:
                if (Inventory.BronzeSwordAmount < 1){
                    Inventory.BronzeSwordAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Bronze Sword"+Main.ANSI_RESET);
                nextWeaponLevel= getWeaponLevel();
                break;
            case 3:
                if (Inventory.SteelSwordAmount < 1){
                    Inventory.SteelSwordAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Steel Sword"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 4:
                if (Inventory.DaggerAmount < 1){
                    Inventory.DaggerAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Dagger"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 5:
                if (Inventory.WoodBowAmount < 1){
                    Inventory.WoodBowAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                    System.out.println(Main.ANSI_BLUE+"You have found a Wood Bow"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 6:
                if (Inventory.EbonyBowAmount < 1){
                    Inventory.EbonyBowAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                    System.out.println(Main.ANSI_BLUE+"You have found a Ebony Bow"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 7:
                if (Inventory.GlassBowAmount < 1){
                    Inventory.GlassBowAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Glass Bow"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 8:
                if (Inventory.WoodStaffAmount < 1){
                    Inventory.WoodStaffAmount++;}
                else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found a Wood Staff"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;
            case 9:
                if (Inventory.EbonyStaffAmount < 1){
                    Inventory.EbonyStaffAmount++;}
                    else {
                    throw new WeaponAlreadyOwnedException("You already own this type of weapon");
                }
                System.out.println(Main.ANSI_BLUE+"You have found an Ebony Staff"+Main.ANSI_RESET);
                nextWeaponLevel = getWeaponLevel();
                break;

        }
    }
    void LootRandomizer(){
        int LootChance = ThreadLocalRandom.current().nextInt(0,100);
        if (LootChance > LootDifficulty){
            try {
                Loot();
            }catch (WeaponAlreadyOwnedException e){

            }
        }
    }//random chance to find something
    private int randomNumberGen(int lBound, int hBound){
        int random;
        random = ThreadLocalRandom.current().nextInt(lBound,hBound);
        return random;
    }

    public void FightMain(){
    }
    static void Die(){
        System.out.println(Main.ANSI_RED + "You have died" + Main.ANSI_RESET);
        System.out.println("You have killed "+DeadEnemyCounter +" enemies");
        Main.CharacterIsALive = false;
    }

    @Override
    public int getDamage() {
        return 0;
    }

    @Override
    public int getCrit() {
        return 0;
    }

    @Override
    public int getCritDmgModifier() {
        return 0;
    }

    @Override
    public int getWeaponLevel() {
        if(Inventory.WeaponEquipped == true){
        return Champion.getInstance().equippedWeapon.getWeaponLevel();
        }
        else {return 1;}
    }

    @Override
    public String getWeaponName() {
        return null;
    }
}
class FightMain{
    FightMain() {
        new Fight();
    }
}

