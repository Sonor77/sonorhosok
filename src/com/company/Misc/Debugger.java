package com.company.Misc;

import com.company.Champion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Debugger {
    static FileOutputStream makeFile() {
        String fileName = Champion.getInstance().getChampName();
        File file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream stream = new FileOutputStream(file);
            return stream;
        } catch (java.io.FileNotFoundException e) {
            System.out.println(e);
            return null;
        }


    }
    public void printToFile(String inputToPrint){
        PrintStream pos = new PrintStream(makeFile());
        System.setOut(pos);
        System.out.println(inputToPrint);
        pos.close();
        try {
            makeFile().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
