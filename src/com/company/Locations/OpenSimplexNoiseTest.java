package com.company.Locations;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

public class OpenSimplexNoiseTest {
    public OpenSimplexNoiseTest() {
        testSimpleNoise();
    }

    /*
     * OpenSimplex Noise sample class.
     */
    private static final int WIDTH = 512;
    private static final int HEIGHT = 512;
    private static final double FEATURE_SIZE = 24;


    private void testSimpleNoise() {
        OpenSimplexNoise noise = new OpenSimplexNoise(); //May provide seed as argument to generate different patterns.
        for (int y = 0; y < 256; y++) {
            for (int x = 0; x < 256; x++) {
                double value = noise.eval(x / 32.0, y / 32.0, 0.5);
            }
        }
    }
}