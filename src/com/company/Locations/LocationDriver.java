package com.company.Locations;

import com.company.Adventure;
import com.company.DBUpdaters;
import com.company.Main;
import com.company.Misc.CrappyMapMissingException;
import com.company.Misc.CrappyTravelException;
import com.company.StatUpdate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class LocationDriver extends MapVo {
    //public static ArrayList<MapVo> Map = new ArrayList<>();
    public static ArrayList<MapVo> ForestLakeMap = new ArrayList<>();
    public static ArrayList<MapVo> CityMap = new ArrayList<>();
    public static MapVo currentMap;

    public void generateMapCity() {//
        int mapSize = 10;
        int fullMapSize = 10;
        int mapX = 0;
        int mapY;
        for (int i = 0; i < fullMapSize; i++) {
            mapX++;
            mapY = 0;
            for (int j = 0; j < mapSize; j++) { //there is a 10,0 location that needs to be removed
                MapVo generatedMap = new MapVo();
                generatedMap.x = mapX;
                generatedMap.y = mapY;
                generatedMap.local = localRandomizerCity();
                new DBUpdaters().saveMap("citymap",generatedMap.x,generatedMap.y,generatedMap.local);
                //CityMap.add(generatedMap);
                mapY++;
            }
        }
    }
    public void generateMapForestLake(int lakeBorder) {//
        int mapSize = 10;
        int fullMapSize = 10;
        int forestLakeMapX = 0;
        int forestLakeMapY;
        for (int i = 0; i < fullMapSize; i++) {
            forestLakeMapX++;
            forestLakeMapY = 0;
            for (int j = 0; j < mapSize; j++) { //there is a 10,0 location that needs to be removed
                MapVo generatedForestMap = new MapVo();
                generatedForestMap.x = forestLakeMapX;
                generatedForestMap.y = forestLakeMapY;
                generatedForestMap.local = localRandomizerForestLake(lakeBorder,forestLakeMapX);
                new DBUpdaters().saveMap("forestlakemap",generatedForestMap.x,generatedForestMap.y,generatedForestMap.local);
                //ForestLakeMap.add(generatedForestMap);
                forestLakeMapY++;
            }
        }
    }
    private String localRandomizerCity(){ //add more colors and locals, maybe even roads
        int random = ThreadLocalRandom.current().nextInt(1,5);
        String local = "";
        switch(random){
            case 1:
                local = "Cave"; // ^
                break;
            case 2:
                local = "Forest"; // %
                break;
            case 3:
                local = "Tower"; // |
                break;
            case 4:
                local = "Pub"; // ß
                break;
            case 5:
                local = "Lake"; // O
                break;
        }
        return local;
    }
    private String localRandomizerForestLake(int lakeBorder, int mapX){ //add more colors and locals, maybe even roads
        double value = openSimplexNoiseProvider();
        boolean subLakeLine = false;
        if(mapX >= lakeBorder){
            subLakeLine = true;
             local = localPickerForestLake(value, subLakeLine);
        }
        else{
            local = localPickerForestLake(value, subLakeLine);
        }
        return local;
    }
    public void move(int nextX, int nextY, ArrayList<MapVo> Map){
        MapVo posHere = getCurrentPos();
        boolean xTrue = false;
        boolean yTrue = false;
        if(posHere.x - 1 == nextX || posHere.x + 1 == nextX || posHere.x == nextX){
            xTrue = true;
        }
        if (posHere.y -1 == nextY || posHere.y + 1 == nextY || posHere.y == nextY){
            yTrue = true;
        }
        if (xTrue == true && yTrue == true){
            int nextLocalNum = new MapVo().getNextLocal(nextX, nextY, Map);
            setCurrentPos(Map.get(nextLocalNum));
        }
        else{System.out.println("Next Location is invalid");}

    }
    public void setCurrentPos(MapVo map){
        currentMap = map;
    }
    public MapVo getCurrentPos(){
        MapVo currentPos = currentMap;
        //System.out.println(currentPos.x+currentPos.y);
        return currentPos;
    }
    public int getCurrentPosX(){
        int retX = 0; //also prone to nullPointer, maybe when the starting map is the city
        try {
            retX = currentMap.x;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retX;
    }
    public int getCurrentPosY(){
        int retY = currentMap.y;
        return retY;
    }
    public void defaultPos(ArrayList<MapVo> Map){

        try {
            currentMap = Map.get(returnIndex(5,5, Map));
        } catch (CrappyMapMissingException e) {
            e.printStackTrace();
        }
    }
    public int returnIndex(int x, int y, ArrayList<MapVo> Map) throws CrappyMapMissingException {
        int index = 0;
        Iterator<MapVo> mapIterator = Map.iterator();//this boi throws nullpointers now.
        int size = Map.size();
        for(int i = 0; i < size; i++){
            MapVo currentMap = mapIterator.next();
            int xCurrent = currentMap.x;
            int yCurrent = currentMap.y;
            if(xCurrent == x && yCurrent == y){
                index = Map.indexOf(currentMap);
                break;
            }
    }
    return index;
}
    public MapVo scanNeighbourPos(MapVo currentMap, ArrayList<MapVo> Map){
        //x a magasság, y a szélesség
        //north = x+1
        //south = x-1
        //west = y-1
        //east = y+1
        MapVo map = null;
        int xSouthNeighbour = currentMap.x-1;
        int xNorthNeighbour = currentMap.x+1;
        int yWestNeighbour = currentMap.x-1;
        int yEastNeighbour = currentMap.x+1;
        ArrayList<Integer> xNeighbourList = new ArrayList<>();
        ArrayList<Integer> yNeighbourList = new ArrayList<>();
        xNeighbourList.add(xNorthNeighbour);
        xNeighbourList.add(xSouthNeighbour);
        yNeighbourList.add(yEastNeighbour);
        yNeighbourList.add(yWestNeighbour);

        for (int xNeighbour : xNeighbourList){
            int xIndex = 0;
            try {
                xIndex = returnIndex(xNeighbour,currentMap.y, Map);
            } catch (CrappyMapMissingException e) {
                e.printStackTrace();
            }
            String neighbouringLocal = getLocal(xIndex, Map);
            if(neighbouringLocal.matches("Pub")){
                map = Map.get(xIndex);
            }
        }
        for (int yNeighbour : yNeighbourList){
            int yIndex = 0;
            try {
                yIndex = returnIndex(currentMap.x, yNeighbour, Map);
            } catch (CrappyMapMissingException e) {
                e.printStackTrace();
            }
            String neighbouringLocal = getLocal(yIndex, Map);
            if(neighbouringLocal.matches("Pub")){
                map = Map.get(yIndex);
            }
        }
        if(map == null){
        //if no pub, go somewhere random
            map = new Adventure().afterAdvantureRandom(Map);
        }

        return map;
    }
    private String localPickerForestLake(double value, boolean lakeLine){
        String local = "";
        if (value <= -0.66 || lakeLine == true){
            local = "Water"; // O
        }
        else if (value <= 0.33){
            local = "Forest"; // %
        }
        else if (value <= 1){
            local = "Roads"; // I
        }
        return local;
    }
    public Double openSimplexNoiseProvider(){ // provides noise between 0 and 1
        double random1 = ThreadLocalRandom.current().nextDouble(0,1);
        double random2 = ThreadLocalRandom.current().nextDouble(0,1);
        double random3 = ThreadLocalRandom.current().nextDouble(0,1);
        double random4 = ThreadLocalRandom.current().nextDouble(0,1);
        OpenSimplexNoise noise = new OpenSimplexNoise();
        double value = noise.eval(random1 / random3, random2 / random4);
        return value;
    }
    public int decideLakeBorder(){
        int random = ThreadLocalRandom.current().nextInt(2,7);
        return random;
    }
    public String startingPointAtRandom(){
        int random = ThreadLocalRandom.current().nextInt(0,10);
        if (random <= 5){
            Main.setMapChoice("ForestLake");
            System.out.println("Starting in the Forest");
            return "ForestLake";
        }
        else {
            Main.setMapChoice("CityMap");
            System.out.println("Starting in the City");
            return "CityMap";}
        }
    public void loadUnloadedMap(){
    if(LocationDriver.ForestLakeMap.isEmpty()){
        new LocationDriver().generateMapForestLake(decideLakeBorder());
    }
    if(LocationDriver.CityMap.isEmpty()){
        new LocationDriver().generateMapCity();
    }
        }
    public ArrayList<MapVo> getCurrentActiveMap(){
        ArrayList<MapVo> currentActiveMap = null;
        if(Main.getMapChoice() == "ForestLake"){
            new StatUpdate().mapSetter("ForestLake");
            currentActiveMap = LocationDriver.ForestLakeMap;
        }
        else if(Main.getMapChoice() == "CityMap"){
            new StatUpdate().mapSetter("CityMap");
            currentActiveMap = LocationDriver.CityMap;
        }
        else {System.out.println("Map is invalid, returning null");}
        return currentActiveMap;
    }
    private boolean detectMapBorder(){
        //values to compare
        Integer x = currentMap.x;
        Integer y = currentMap.y;

        if(y == 0 || y == 10 || x == 0 || x == 10){//if you are on a border
            return true;
        }
        else {return false;}
    }
    private void changeCurrentMap(boolean onBorder) throws CrappyTravelException {
        if(onBorder == true){
            String mapChoice = Main.getMapChoice();
            if (mapChoice == "ForestLake"){
                Main.setMapChoice("CityMap");
                Adventure.currentMap = LocationDriver.CityMap;
                try {
                    if(currentMap.equals(null)){
                        throw new CrappyMapMissingException("Loading the map has failed");
                    }
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                defaultPos(LocationDriver.CityMap);
                System.out.println("You have travelled to the city");
            }
            else if (mapChoice == "CityMap"){
                Main.setMapChoice("ForestLakeMap");
                Adventure.currentMap = LocationDriver.ForestLakeMap;
                try {
                    if(currentMap.equals(null)){
                        throw new CrappyMapMissingException("Loading the map has failed");
                    }
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                defaultPos(LocationDriver.ForestLakeMap);
                System.out.println("You have travelled to the Forest");
            }
            else{throw new CrappyTravelException("mapChoice doesn't seem to be valid");}
        }
    }
    public void changeMapOnBorder(){
        boolean onBorder = detectMapBorder();
        try {
            changeCurrentMap(onBorder);
        }catch (CrappyTravelException e){System.out.println(e);}
    }
}
