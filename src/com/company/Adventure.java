package com.company;

import com.company.Locations.DrawMap;
import com.company.Locations.LocationDriver;
import com.company.Locations.MapVo;
import com.company.Misc.CrappyMapMissingException;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static com.company.Sets.TowerLevel;

public class Adventure { //moving between tiles and local drived action happens here
    MapVo currentPos = new LocationDriver().getCurrentPos();
    public static String currentLocal;
    public static ArrayList<MapVo> currentMap;

    public Adventure() {
        //System.out.println("Current Local "+currentLocal);
        //System.out.println("Current Pos " + new LocationDriver().getCurrentPosX() + new LocationDriver().getCurrentPosY());


    }

    public void nextAdventure(ArrayList<MapVo> Map) {
        currentMap = new LocationDriver().getCurrentActiveMap();
        try {
            currentLocal = currentPos.getLocal(new LocationDriver().returnIndex(new LocationDriver().getCurrentPosX(), new LocationDriver().getCurrentPosY(),currentMap),currentMap);
        } catch (CrappyMapMissingException e) {
            e.printStackTrace();
        }
        new DrawMap().drawMap();
        System.out.println("---------------------------------------");
        switch (currentLocal) {
            case "Cave":
                System.out.println(Main.ANSI_RED + "You enter a dark Cave" + Main.ANSI_RESET);
                new Fight();
                break;
            case "Pub":
                System.out.println(Main.ANSI_GREEN + "You take a long rest in a Pub, where you forget about your wounds" + Main.ANSI_RESET);
                StatUpdate.HealUp();
                break;
            case "Forest":
                System.out.println(Main.ANSI_RED + "You enter a deep, dark Forest" + Main.ANSI_RESET);
                new Fight();
                break;
            case "Tower":
                Fight.LootDifficulty = 40;
                new MobLeveler().TowerLeveler();
                System.out.println(Main.ANSI_RED + "You have found a Tower. It has " + TowerLevel + " levels. Prepare for a long battle" + Main.ANSI_RESET);
                boolean towerIsDone = false;
                while (Main.CharacterIsALive == true && towerIsDone == false) {
                    Fight.LootDifficulty = 60;
                    for (int i = 1; i <= TowerLevel; i++) {
                        System.out.println("This is the " + i + "th level of this tower.");
                        new Fight();
                        if (Main.CharacterIsALive == false) {
                            return;
                        }
                    }
                    towerIsDone = true;
                }
                if (Main.CharacterIsALive == true) {
                    System.out.println("After such a heavy fight, you sit down at the root of the tower, and rest");
                    StatUpdate.HealUp();
                }
                break;
            case "Lake":
                System.out.println("You always loved fishing");
                int random = ThreadLocalRandom.current().nextInt(0, 10);
                if (random < 5) {
                    System.out.println("God you have caught a huge fish");
                    StatUpdate.HealUp();
                }
                break;
        }
        afterAdvanture();
    }

    //x a magasság, y a szélesség
    //north = x+1
    //south = x-1
    //west = y-1
    //east = y+1
    //currentPos could be colored
    private void afterAdvanture() {//hangs again
        int random = ThreadLocalRandom.current().nextInt(0, 12);
        int mapX = new LocationDriver().getCurrentPosX();
        int mapY = new LocationDriver().getCurrentPosY();
        int xMapborder = 0;
        int yMapborder = 0;
        boolean locationValid = false;
        while (locationValid == false) {
            if (random <= 3) {
                //north
                if (mapX + 1 >= xMapborder && mapY >= yMapborder) {
                    new LocationDriver().move(mapX + 1, mapY, currentMap);
                    locationValid = true;
                    new LocationDriver().changeMapOnBorder();
                }
            } else if (random < 6 && random > 3) {
                //south
                if (mapX - 1 >= xMapborder && mapY + 1 >= yMapborder) {
                    new LocationDriver().move(mapX - 1, mapY, currentMap);
                    locationValid = true;
                    new LocationDriver().changeMapOnBorder();
                }
            } else if (random <= 9 && random > 6) {
                //west
                if (mapX >= xMapborder && mapY - 1 >= yMapborder) {
                    new LocationDriver().move(mapX, mapY - 1, currentMap);
                    locationValid = true;
                    new LocationDriver().changeMapOnBorder();
                }
            } else if (random > 9) {
                //east
                if (mapX >= xMapborder && mapY + 1 >= yMapborder) {
                    new LocationDriver().move(mapX, mapY + 1, currentMap);
                    locationValid = true;
                    new LocationDriver().changeMapOnBorder();
                }
            }
        }
    }

    public MapVo afterAdvantureRandom(ArrayList<MapVo> currentMap) {
        int random = ThreadLocalRandom.current().nextInt(0, 12);
        int mapX = new LocationDriver().getCurrentPosX();
        int mapY = new LocationDriver().getCurrentPosY();
        int xMapborder = 0;
        int yMapborder = 0;
        int index = 0;
        if (random <= 3) {
            //north
            if (mapX + 1 >= xMapborder && mapY >= yMapborder) {
                try {
                    index = new LocationDriver().returnIndex(mapX + 1, mapY, currentMap);
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                return currentMap.get(index);
            }
        } else if (random < 6 && random > 3) {
            //south
            if (mapX - 1 >= xMapborder && mapY + 1 >= yMapborder) {
                try {
                    index = new LocationDriver().returnIndex(mapX - 1, mapY, currentMap);
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                return currentMap.get(index);
            }
        } else if (random <= 9 && random > 6) {
            //west
            if (mapX >= xMapborder && mapY - 1 >= yMapborder) {
                try {
                    index = new LocationDriver().returnIndex(mapX, mapY-1, currentMap);
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                return currentMap.get(index);
            }
        } else if (random > 9) {
            //east
            if (mapX >= xMapborder && mapY + 1 >= yMapborder) {
                try {
                    index = new LocationDriver().returnIndex(mapX, mapY+ 1, currentMap);
                } catch (CrappyMapMissingException e) {
                    e.printStackTrace();
                }
                return currentMap.get(index);
            }
        }
        return null; //if it's so broke, let is die.
    }
}